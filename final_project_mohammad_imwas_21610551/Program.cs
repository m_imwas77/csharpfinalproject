﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace final_project_mohammad_imwas_21610551
{
    static class Program
    {
        public static int LoginUserId ;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            LoginFrame loginFrame = new LoginFrame();
            Application.Run(loginFrame);
            

            if (loginFrame.auth)
            {
                LoginUserId = loginFrame.userId;
                
                if(loginFrame.type == 0)
                {
                    Application.Run(new AdminPage());
                }
                if(loginFrame.type == 1)
                {
                    Application.Run(new DoctorPage());
                }
                if(loginFrame.type == 2)
                {
                    Application.Run(new SecretaryPage());
                }
            }
        }
    }
}
