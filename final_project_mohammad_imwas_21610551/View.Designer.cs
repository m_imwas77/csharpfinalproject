﻿namespace final_project_mohammad_imwas_21610551
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbxChild = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbxDoctors = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.cmbVistStatus = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chSingel = new System.Windows.Forms.CheckBox();
            this.chdDauble = new System.Windows.Forms.CheckBox();
            this.txtDateSingle = new System.Windows.Forms.DateTimePicker();
            this.txtDateDouble = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbxChild
            // 
            this.cmbxChild.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxChild.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbxChild.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxChild.FormattingEnabled = true;
            this.cmbxChild.Location = new System.Drawing.Point(522, 9);
            this.cmbxChild.Name = "cmbxChild";
            this.cmbxChild.Size = new System.Drawing.Size(174, 27);
            this.cmbxChild.TabIndex = 47;
            this.cmbxChild.SelectedIndexChanged += new System.EventHandler(this.cmbxChild_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(702, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 27);
            this.label3.TabIndex = 48;
            this.label3.Text = "المريض";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbxDoctors
            // 
            this.cmbxDoctors.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxDoctors.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbxDoctors.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxDoctors.FormattingEnabled = true;
            this.cmbxDoctors.Location = new System.Drawing.Point(259, 8);
            this.cmbxDoctors.Name = "cmbxDoctors";
            this.cmbxDoctors.Size = new System.Drawing.Size(156, 27);
            this.cmbxDoctors.TabIndex = 45;
            this.cmbxDoctors.SelectedIndexChanged += new System.EventHandler(this.cmbxDoctors_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(421, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 27);
            this.label1.TabIndex = 46;
            this.label1.Text = "الطبيب";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedHeaders;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 195);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(776, 243);
            this.dataGridView1.TabIndex = 49;
            // 
            // cmbVistStatus
            // 
            this.cmbVistStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVistStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbVistStatus.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVistStatus.FormattingEnabled = true;
            this.cmbVistStatus.Location = new System.Drawing.Point(12, 8);
            this.cmbVistStatus.Name = "cmbVistStatus";
            this.cmbVistStatus.Size = new System.Drawing.Size(135, 27);
            this.cmbVistStatus.TabIndex = 50;
            this.cmbVistStatus.SelectedIndexChanged += new System.EventHandler(this.cmbVistStatus_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(153, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 27);
            this.label2.TabIndex = 51;
            this.label2.Text = "حاله الحجز";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chSingel
            // 
            this.chSingel.AutoSize = true;
            this.chSingel.Location = new System.Drawing.Point(662, 66);
            this.chSingel.Name = "chSingel";
            this.chSingel.Size = new System.Drawing.Size(126, 17);
            this.chSingel.TabIndex = 52;
            this.chSingel.Text = "البحث من خلال التاريخ";
            this.chSingel.UseVisualStyleBackColor = true;
            this.chSingel.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // chdDauble
            // 
            this.chdDauble.AutoSize = true;
            this.chdDauble.Location = new System.Drawing.Point(536, 66);
            this.chdDauble.Name = "chdDauble";
            this.chdDauble.Size = new System.Drawing.Size(101, 17);
            this.chdDauble.TabIndex = 53;
            this.chdDauble.Text = "البحث في الفتره";
            this.chdDauble.UseVisualStyleBackColor = true;
            this.chdDauble.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // txtDateSingle
            // 
            this.txtDateSingle.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDateSingle.Location = new System.Drawing.Point(662, 90);
            this.txtDateSingle.Name = "txtDateSingle";
            this.txtDateSingle.Size = new System.Drawing.Size(126, 20);
            this.txtDateSingle.TabIndex = 54;
            // 
            // txtDateDouble
            // 
            this.txtDateDouble.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDateDouble.Location = new System.Drawing.Point(536, 90);
            this.txtDateDouble.Name = "txtDateDouble";
            this.txtDateDouble.Size = new System.Drawing.Size(101, 20);
            this.txtDateDouble.TabIndex = 55;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtDateDouble);
            this.Controls.Add(this.txtDateSingle);
            this.Controls.Add(this.chdDauble);
            this.Controls.Add(this.chSingel);
            this.Controls.Add(this.cmbVistStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cmbxChild);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbxDoctors);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "View";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "الحجوزات";
            this.Load += new System.EventHandler(this.View_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbxChild;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbxDoctors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox cmbVistStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chSingel;
        private System.Windows.Forms.CheckBox chdDauble;
        private System.Windows.Forms.DateTimePicker txtDateSingle;
        private System.Windows.Forms.DateTimePicker txtDateDouble;
    }
}