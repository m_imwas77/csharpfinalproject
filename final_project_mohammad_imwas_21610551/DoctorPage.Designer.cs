﻿namespace final_project_mohammad_imwas_21610551
{
    partial class DoctorPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnUserModifay = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(492, 40);
            this.label1.TabIndex = 2;
            this.label1.Text = "صفحه الطبيب";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUserModifay
            // 
            this.btnUserModifay.Location = new System.Drawing.Point(344, 61);
            this.btnUserModifay.Name = "btnUserModifay";
            this.btnUserModifay.Size = new System.Drawing.Size(160, 107);
            this.btnUserModifay.TabIndex = 8;
            this.btnUserModifay.Text = "اداره المراجعات ";
            this.btnUserModifay.UseVisualStyleBackColor = true;
            this.btnUserModifay.Click += new System.EventHandler(this.btnUserModifay_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(178, 61);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(160, 107);
            this.button1.TabIndex = 9;
            this.button1.Text = "تفاصيل الجلسات";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 61);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(160, 107);
            this.button2.TabIndex = 10;
            this.button2.Text = "الخروج";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // DoctorPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 172);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnUserModifay);
            this.Controls.Add(this.label1);
            this.Name = "DoctorPage";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "صفحه الطبيب";
            this.Load += new System.EventHandler(this.DoctorPage_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUserModifay;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}