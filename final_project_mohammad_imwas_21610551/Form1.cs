﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using final_project_mohammad_imwas_21610551.model;
using final_project_mohammad_imwas_21610551.Enum;
namespace final_project_mohammad_imwas_21610551
{
    public partial class LoginFrame : Form
    {
        public   bool auth = false;
        public   int type = -1;
        public int userId = -1;
        public LoginFrame()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string UserName = txtUserName.Text;
            string PassWord = txtPassword.Text;
            using(ModelContext db = new ModelContext())
            {

                DateTime dt1 ;
                string datestring   = DateTime.Now.ToShortDateString();
                DateTime dt2 = DateTime.Parse(datestring);
                List<Visit> visits = db.visits.ToList<Visit>();

                foreach (var item in visits)
                {
                    dt1 = DateTime.Parse(item.VisitDate);
                   
                    if (dt1 < dt2 && item.VisitStatus != VisitStatus.COMPLETED)
                    {
                        item.VisitStatus = VisitStatus.LATE;
                    }
                }
                db.SaveChanges();
                User user = db.Users
                                    .Where(s => s.UserName == UserName
                                            && s.PassWord == PassWord 
                                            && s.Active == true)
                                    .FirstOrDefault<User>();

                if(user == null)
                {
                    MessageBox.Show("اسم المستخدم او كلمه المرور خاطئه");
                    txtUserName.Focus();
                    return;
                }

                auth = true;
                userId = user.Id;
                if (user.Type == UserType.ADMIN)
                {
                    type = 0;
                }
                if (user.Type == UserType.DOCTOR)
                {
                    type = 1;
                }
                if (user.Type == UserType.SECRETARY)
                {
                    type = 2;
                }

                Close();

                
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
