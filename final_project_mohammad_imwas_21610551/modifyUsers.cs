﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using final_project_mohammad_imwas_21610551.model;
using final_project_mohammad_imwas_21610551.Enum;
using System.Data.Entity.Migrations;
namespace final_project_mohammad_imwas_21610551
{
    public partial class modifyUsers : Form
    {
        public modifyUsers()
        {
            InitializeComponent();
        }
        private void ReRenderDataGridView()
        {
            using (ModelContext db = new ModelContext())
            {

                dataGridView1.DataSource = db.Users.ToList<User>();
                ModifyDataGridView();
            }
        }
        private void ModifyDataGridView()
        {
            dataGridView1.Columns.Remove("id");
            dataGridView1.Columns.Remove("Visits");
            dataGridView1.Columns[0].HeaderText = "اسم المستخدم";
            dataGridView1.Columns[1].HeaderText = "كلمه المرور";
            dataGridView1.Columns[2].HeaderText = "الصلاحيه";
            dataGridView1.Columns[3].HeaderText = "فعال";
            dataGridView1.Columns[4].HeaderText = "الاسم الاول";
            dataGridView1.Columns[5].HeaderText = "اسم العائله";
            dataGridView1.Columns[6].HeaderText = "تاريخ الميلاد";
            dataGridView1.Columns[7].HeaderText = "الجنس";
            dataGridView1.Columns[8].HeaderText = "رقم الهاتف";
            dataGridView1.Columns[9].HeaderText = "التخصص";
            for (int i = 0; i <= 9; i++)
            {
                dataGridView1.Columns[i].ReadOnly = true;
            }
        }
        private void modifyUsers_Load(object sender, EventArgs e)
        {
            ReRenderDataGridView();

            cmbxType.Items.Add(UserType.ADMIN);
            cmbxType.Items.Add(UserType.DOCTOR);
            cmbxType.Items.Add(UserType.SECRETARY);
            cmbxType.SelectedIndex = 0;
            
            cmbxActive.Items.Add("فعال");
            cmbxActive.Items.Add("غيرفعال");
            cmbxActive.SelectedIndex = 0;

            comboBox1.Items.Add(Gender.MALE);
            comboBox1.Items.Add(Gender.FEMALE);
            comboBox1.Items.Add(Gender.BOTH);
            comboBox1.SelectedIndex = -1;

            cmbxGender.Items.Add(Gender.MALE);
            cmbxGender.Items.Add(Gender.FEMALE);
            cmbxGender.SelectedIndex = 0;

            txtDOB.MaxDate = System.DateTime.Now.AddYears(-21);
            txtDOB.MinDate = System.DateTime.Now.AddYears(-101);

            //txtPhoneNumber.AllowPromptAsInput = true;

        }

        private void cmbxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        
        private bool EditAddValidation(string typeOf)
        {
            int numberOfValidation = -6;
            if(typeOf.ToUpper() == "EDIT")
            {
                numberOfValidation = -5;
            }

            int ShowErrorWhenAdd = 0;
            if (txtUserName.Text == "")
            {
                errorProvider1.SetError(txtUserName, "اسم المتخدم مطلوب");
                txtUserName.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtUserName, "");
                ShowErrorWhenAdd--;
            }
            if (typeOf.ToUpper() != "EDIT")
            {
                using (ModelContext db = new ModelContext())
                {
                    User user = db.Users.Where(c => c.UserName == txtUserName.Text).FirstOrDefault<User>();

                    if (user != null)
                    {
                        errorProvider1.SetError(txtUserName, "اسم المستخدم مستخدم ب الفعل");
                        txtUserName.Focus();
                        ShowErrorWhenAdd++;
                    }
                    else
                    {
                        errorProvider1.SetError(txtUserName, "");
                        ShowErrorWhenAdd--;
                    }
                }
            }
            if (txtPassword.Text == "")
            {
                errorProvider1.SetError(txtPassword, "كلمه المرور مطلوب");
                txtPassword.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtPassword, "");
                ShowErrorWhenAdd--;
            }

            if (txtFirstName.Text == "")
            {
                errorProvider1.SetError(txtFirstName, "الاسم الاول مطلوب");
                txtFirstName.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtFirstName, "");
                ShowErrorWhenAdd--;
            }

            if (txtLastName.Text == "")
            {
                errorProvider1.SetError(txtLastName, "الاسم الاول مطلوب");
                txtLastName.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtLastName, "");
                ShowErrorWhenAdd--;
            }

            if (!txtPhoneNumber.MaskCompleted)
            {
                errorProvider1.SetError(txtPhoneNumber, "اكمل رقم الهاتف");
                txtPhoneNumber.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtPhoneNumber, "");
                ShowErrorWhenAdd--;
            }

            if (txtMojer.Text == "")
            {
                errorProvider1.SetError(txtMojer, "التخصص مطلوب");
                txtMojer.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtMojer, "");
                ShowErrorWhenAdd--;
            }
            if (ShowErrorWhenAdd > numberOfValidation)return  true;
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (EditAddValidation("Add"))
            {
                return;
            }
            using(ModelContext db = new ModelContext())
            {
                db.Users.Add(GetUser());
                db.SaveChanges();
            }
            ReRenderDataGridView();
            ClearForme();
        }
        
        private User GetUser()
        {

            User user = new User()
            {
                UserName = txtUserName.Text,
                PassWord = txtPassword.Text,
                Type = (cmbxType.SelectedIndex == 0) ? UserType.ADMIN
                                                     : (cmbxType.SelectedIndex == 1)
                                                     ? UserType.DOCTOR
                                                     : UserType.SECRETARY,
                Active = (cmbxActive.SelectedIndex == 0) ? true : false,
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                DateOfBirth = txtDOB.Text,
                Gender = (cmbxGender.SelectedIndex == 0) ? Gender.MALE : Gender.FEMALE,
                PhoneNumber = txtPhoneNumber.Text,
                Mojer = txtMojer.Text
            };

            return user;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == -1)
            {


                try
                {
                    txtUserName.Text = dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                    txtPassword.Text = dataGridView1.Rows[e.RowIndex].Cells[1].FormattedValue.ToString();
                    cmbxType.SelectedIndex = GetTypeIndex(dataGridView1.Rows[e.RowIndex].Cells[2].FormattedValue.ToString());
                    cmbxActive.SelectedIndex = GetActiveIndex(dataGridView1.Rows[e.RowIndex].Cells[3].FormattedValue.ToString());
                    txtFirstName.Text = dataGridView1.Rows[e.RowIndex].Cells[4].FormattedValue.ToString();
                    txtLastName.Text = dataGridView1.Rows[e.RowIndex].Cells[5].FormattedValue.ToString();
                    txtDOB.Text = dataGridView1.Rows[e.RowIndex].Cells[6].FormattedValue.ToString();
                    cmbxGender.SelectedIndex = GetGenderIndex(dataGridView1.Rows[e.RowIndex].Cells[7].FormattedValue.ToString());
                    txtPhoneNumber.Text = dataGridView1.Rows[e.RowIndex].Cells[8].FormattedValue.ToString();
                    txtMojer.Text = dataGridView1.Rows[e.RowIndex].Cells[9].FormattedValue.ToString();
                    btnDelete.Enabled = true;
                    btnNew.Enabled = true;
                    btnUpdate.Enabled = true;
                    btnAdd.Enabled = false;
                    txtUserName.Enabled = false;
                    txtPassword.Enabled = false;
                }
                catch (Exception)
                {
                        MessageBox.Show("الرجاء اختيار عنصر واحد");
                }
            }
        }

        private int GetTypeIndex(string s)
        {
            if (s == "ADMIN") return 0;
            if (s == "DOCTOR") return 1;
            return 2;
        }

        private int GetActiveIndex(string s)
        {
            if (s.ToUpper() == "TRUE") return 0;
            return 1;
        }

        private int GetGenderIndex(string s)
        {
            if (s == "MALE") return 0;
            return 1;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            using(ModelContext db = new ModelContext())
            {
                User user = db.Users.Where(c => c.UserName == txtUserName.Text).FirstOrDefault<User>();
                if(user != null)
                {
                    DialogResult result = MessageBox.Show("هل انت متاكد من الحذف", "حذف مستخدم", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                    if (result == DialogResult.Yes)
                    {

                        List<Visit> visits = db.visits.Where(c => c.User.UserName == txtUserName.Text).ToList<Visit>();
                        foreach (var item in visits)
                        {
                            List<VisitDescription> visitDescriptions = db.VisitDescriptions.Where(c => c.Visit.Id == item.Id).ToList<VisitDescription>();
                            List<Medicine> medicines = db.Medicines.Where(c => c.Visit.Id == item.Id).ToList<Medicine>();
                            List<Examination> examinations = db.Examinations.Where(c => c.Visit.Id == item.Id).ToList<Examination>();
                            foreach (var item1 in visitDescriptions)
                            {
                                db.VisitDescriptions.Remove(item1);
                            }
                            foreach (var item2 in medicines)
                            {
                                db.Medicines.Remove(item2);
                            }
                            foreach (var item3 in examinations)
                            {
                                db.Examinations.Remove(item3);
                            }
                            db.SaveChanges();
                            db.visits.Remove(item);
                        }
                        db.SaveChanges();
                        db.Users.Remove(user);

                        db.SaveChanges();
                        ClearForme();
                        ReRenderDataGridView();
                    }
                }
                else
                {
                    MessageBox.Show("لم يتم العثور على المستخدم");
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (EditAddValidation("EDIT"))
            {
                return;
            }
            using (ModelContext db = new ModelContext())
            {
                User user = db.Users.Where(c => c.UserName == txtUserName.Text).FirstOrDefault<User>();
                if (user != null)
                {
                    user.Type = (cmbxType.SelectedIndex == 0) ? UserType.ADMIN
                                                     : (cmbxType.SelectedIndex == 1)
                                                     ? UserType.DOCTOR
                                                     : UserType.SECRETARY;
                    user.Active = (cmbxActive.SelectedIndex == 0) ? true : false;
                    user.FirstName = txtFirstName.Text;
                    user.LastName = txtLastName.Text;
                    user.DateOfBirth = txtDOB.Text;
                    user.Gender = (cmbxGender.SelectedIndex == 0) ? Gender.MALE : Gender.FEMALE;
                    user.PhoneNumber = txtPhoneNumber.Text;
                    user.Mojer = txtMojer.Text;
                    db.SaveChanges();
                    ClearForme();
                    ReRenderDataGridView();
                }
                else
                {
                    MessageBox.Show("لم يتم العثور على المستخدم");
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            ClearForme();
        }

        private void ClearForme()
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
            cmbxActive.SelectedIndex = 0;
            cmbxGender.SelectedIndex = 0;
            cmbxType.SelectedIndex = 0;
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtDOB.Text = System.DateTime.Now.AddYears(-100).ToShortDateString(); 
            txtPhoneNumber.Text = "";
            txtMojer.Text = "";
            btnDelete.Enabled = false;
            btnUpdate.Enabled = false;
            btnNew.Enabled = false;
            btnAdd.Enabled = true;
            txtUserName.Enabled = true;
            txtPassword.Enabled = true;

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            using(ModelContext db = new ModelContext())
            {
                if (comboBox1.SelectedIndex == 2)
                { 
                    dataGridView1.DataSource = db.Users
                                                 .Where(c => (c.UserName.Contains(txtSearch.Text)
                                                        || c.PassWord.Contains(txtSearch.Text)
                                                        || c.FirstName.Contains(txtSearch.Text)
                                                        || c.LastName.Contains(txtSearch.Text)
                                                        || c.Mojer.Contains(txtSearch.Text)
                                                        || c.PhoneNumber.Contains(txtSearch.Text)
                                                        || c.DateOfBirth.Contains(txtSearch.Text))
                                                        && c.Gender == ((comboBox1.SelectedIndex == 0) ? Gender.MALE : Gender.FEMALE)).ToList<User>();
                }
                else
                {
                    dataGridView1.DataSource = db.Users
                                             .Where(c => (c.UserName.Contains(txtSearch.Text)
                                                    || c.PassWord.Contains(txtSearch.Text)
                                                    || c.FirstName.Contains(txtSearch.Text)
                                                    || c.LastName.Contains(txtSearch.Text)
                                                    || c.Mojer.Contains(txtSearch.Text)
                                                    || c.PhoneNumber.Contains(txtSearch.Text)
                                                    || c.DateOfBirth.Contains(txtSearch.Text))
                                                    ).ToList<User>();

                }
                ModifyDataGridView();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            using (ModelContext db = new ModelContext())
            {
                if(comboBox1.SelectedIndex == 2)
                {
                    dataGridView1.DataSource = db.Users.ToList<User>();
                }
                else
                    dataGridView1.DataSource = db.Users
                                             .Where(c=>c.Gender == ((comboBox1.SelectedIndex == 0) ?Gender.MALE:Gender.FEMALE) ).ToList<User>();
                ModifyDataGridView();
            }
        }
    }
}
