﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace final_project_mohammad_imwas_21610551.Enum
{
    public enum VisitStatus
    {
        COMPLETED = 0,
        INPROGRESS = 1,
        LATE = 2
    }
}
