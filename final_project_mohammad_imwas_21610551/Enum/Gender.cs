﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace final_project_mohammad_imwas_21610551.Enum
{
    public enum Gender
    {
        MALE = 0,
        FEMALE = 1,
        BOTH = 2
    }
}
