﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using final_project_mohammad_imwas_21610551.model;
using final_project_mohammad_imwas_21610551.Enum;
namespace final_project_mohammad_imwas_21610551
{
    public partial class EditSession : Form
    {
        public EditSession()
        {
            InitializeComponent();
        }

        private void EditSession_Load(object sender, EventArgs e)
        {
            using(ModelContext db  = new ModelContext())
            {
                List<SelectedChild1> selectedChildren =
                     db.visits.Where(c => c.User.Id == Program.LoginUserId
                                     && c.VisitStatus == VisitStatus.COMPLETED)
                                     .Select(p => new SelectedChild1()
                                     {
                                         Id = p.Child.Id,
                                         FirstName = p.Child.FirstName
                                     }).ToList<SelectedChild1>();

                cmbxChild.DataSource = selectedChildren;
                cmbxChild.DisplayMember = "FirstName";
                cmbxChild.ValueMember = "Id";
                cmbxChild.SelectedIndex = -1;
                ComboBox CB = new ComboBox();
                CB.Items.Add("Description");
                CB.Items.Add("Medicine");
                CB.Items.Add("Examination");
                CB.SelectedIndex = 0;
                ((DataGridViewComboBoxColumn)dataGridView1.Columns[2]).DataSource = CB.Items;

                cmbxType.Items.Add("Description");
                cmbxType.Items.Add("Medicine");
                cmbxType.Items.Add("Examination");
                cmbxType.SelectedIndex = 0;
                
                dvgVists.Enabled = false;
                btnAdd.Enabled = false;
                
            }
        }

        private void cmbxChild_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnAdd.Enabled = true;
            using (ModelContext db = new ModelContext())
            {
                try
                {
                    Child child = db.Children.Where(c => c.Id.ToString() == cmbxChild.SelectedValue.ToString()).FirstOrDefault<Child>();
                    if (child != null)
                    {
                        txtFirtName.Text = child.FirstName;
                        txtLastName.Text = child.LastName;
                        txtGender.Text = (child.Gender == Gender.MALE) ? "male" : "female";
                        txtDOB.Text = child.DateOfBirth;
                        dvgVists.Enabled = true;
                        List<VistSessionSelected> visits = db.visits.Where(c => c.Child.Id == child.Id)
                                                .Select(p => new VistSessionSelected() {
                                                    Date = p.VisitDate
                                                }).ToList<VistSessionSelected>();

                        dvgVists.DataSource = visits;
                        
                    }
                    else
                    {
                        MessageBox.Show("لم يتم اختيار اي طفل");
                    }
                }
                catch (Exception) { }
            }
        }
        string date = "";
        private void dvgVists_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1)
            {
                try
                {
                    using (ModelContext db = new ModelContext())
                    {
                        date = dvgVists.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                        List<VisitDescription> visitDescriptions = db.VisitDescriptions.Where(c => c.Visit.VisitDate == date).ToList<VisitDescription>();
                        List<Examination> examinations = db.Examinations.Where(c => c.Visit.VisitDate == date).ToList<Examination>();
                        List<Medicine> medicines = db.Medicines.Where(c => c.Visit.VisitDate == date).ToList<Medicine>();
                      
                        foreach (var item in visitDescriptions)
                        {
                            dataGridView1.Rows.Add(item.Name, item.Description, "Description",item.Id);
                        }
                        foreach (var item in examinations)
                        {

                            dataGridView1.Rows.Add(item.Name, item.Description, "Examination", item.Id);
                        }
                        foreach (var item in medicines)
                        {
                            dataGridView1.Rows.Add(item.Name, item.Description, "Medicine", item.Id);
                        }
                        dataGridView1.Enabled = true;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("الرجاء اختيار عنصر واحد");
                }
            }
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.FormattedValue.ToString() == "")
            {
                e.Cancel = true;
                dataGridView1.Rows[e.RowIndex].ErrorText = "القيمه مطولبه";
            }
            else
            {
                dataGridView1.Rows[e.RowIndex].ErrorText = "";
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(textBox1.Text == "")
            {
                errorProvider1.SetError(textBox1, "مطلوب");
                return;
            }
            else
            {
                errorProvider1.SetError(textBox1, "");
            }

            if (textBox2.Text == "")
            {
                errorProvider1.SetError(textBox2, "مطلوب");
                return;
            }
            else
            {
                errorProvider1.SetError(textBox2, "");
            }

            string type = "";
            if (cmbxType.SelectedIndex == 0)
            {
                type = "Description";
            }
            else if(cmbxType.SelectedIndex == 1)
            {
                type = "Medicine";
            }
            else
            {
                type = "Examination";
            }

            dataGridView1.Rows.Add(textBox1.Text, textBox2.Text, type, -1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (date != "") {
                DialogResult result = MessageBox.Show("هل متاكد", "حفظ التعديلات", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (result == DialogResult.Yes)
                {
                    using (ModelContext db = new ModelContext())
                    {
                        Visit visit = db.visits.Where(c => c.Child.Id.ToString() == cmbxChild.SelectedValue.ToString()
                                                       && c.VisitDate == date).FirstOrDefault<Visit>();

                        if (visit != null)
                        {
                            foreach (DataGridViewRow item in dataGridView1.Rows)
                            {
                                if (item.Cells[0].FormattedValue.ToString() == "") break;
                                if (item.Cells[1].FormattedValue.ToString() == "") break;
                                if (item.Cells[2].FormattedValue.ToString() == "") break;
                                if (item.Cells[2].FormattedValue.ToString() == "Description")
                                {
                                    if (item.Cells[3].FormattedValue.ToString() == "-1")
                                    {
                                        VisitDescription description = new VisitDescription()
                                        {
                                            Name = item.Cells[0].FormattedValue.ToString(),
                                            Description = item.Cells[1].FormattedValue.ToString(),
                                            Visit = visit
                                        };
                                        db.VisitDescriptions.Add(description);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        string x = item.Cells[3].FormattedValue.ToString();
                                        VisitDescription visitDescription = db.VisitDescriptions.Where(c => c.Id.ToString() == x).FirstOrDefault<VisitDescription>();
                                        visitDescription.Name = item.Cells[0].FormattedValue.ToString();
                                        visitDescription.Description = item.Cells[1].FormattedValue.ToString();
                                        db.SaveChanges();
                                    }
                                }
                                else if (item.Cells[2].FormattedValue.ToString() == "Medicine")
                                {
                                    if (item.Cells[3].FormattedValue.ToString() == "-1")
                                    {
                                        Medicine medicine = new Medicine()
                                        {
                                            Name = item.Cells[0].FormattedValue.ToString(),
                                            Description = item.Cells[1].FormattedValue.ToString(),
                                            Visit = visit
                                        };
                                        db.Medicines.Add(medicine);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        string x = item.Cells[3].FormattedValue.ToString();
                                        Medicine medicine = db.Medicines.Where(c => c.Id.ToString() == x).FirstOrDefault<Medicine>();
                                        medicine.Name = item.Cells[0].FormattedValue.ToString();
                                        medicine.Description = item.Cells[1].FormattedValue.ToString();
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    if (item.Cells[3].FormattedValue.ToString() == "-1")
                                    {
                                        Examination examination = new Examination()
                                        {
                                            Name = item.Cells[0].FormattedValue.ToString(),
                                            Description = item.Cells[1].FormattedValue.ToString(),
                                            Visit = visit
                                        };
                                        db.Examinations.Add(examination);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        string x = item.Cells[3].FormattedValue.ToString();
                                        Examination examination = db.Examinations.Where(c => c.Id.ToString() == x).FirstOrDefault<Examination>();
                                        examination.Name = item.Cells[0].FormattedValue.ToString();
                                        examination.Description = item.Cells[1].FormattedValue.ToString();
                                        db.SaveChanges();
                                    }
                                }

                            }
                            MessageBox.Show("تم التعديل بنجاح");
                        }
                        else
                        {
                            MessageBox.Show("لم يتم الاختيار");
                        }

                    }
                }
            }
            else
            {
                MessageBox.Show("لم يتم الاختيار");
            }
        }
    }
    public class VistSessionSelected
    {
        public string Date { get; set; }
    }
    public class SelectedChild1
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
    }
}
