﻿namespace final_project_mohammad_imwas_21610551
{
    partial class Reservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbxDoctors = new System.Windows.Forms.ComboBox();
            this.cmbxChild = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnAddNewRes = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtNew = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtDate
            // 
            this.txtDate.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDate.Location = new System.Drawing.Point(9, 9);
            this.txtDate.MinDate = new System.DateTime(1910, 1, 1, 0, 0, 0, 0);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(135, 27);
            this.txtDate.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(150, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 27);
            this.label2.TabIndex = 33;
            this.label2.Text = "تاريخ الحجز";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(419, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 27);
            this.label1.TabIndex = 31;
            this.label1.Text = "الطبيب";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbxDoctors
            // 
            this.cmbxDoctors.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxDoctors.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbxDoctors.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxDoctors.FormattingEnabled = true;
            this.cmbxDoctors.Location = new System.Drawing.Point(278, 8);
            this.cmbxDoctors.Name = "cmbxDoctors";
            this.cmbxDoctors.Size = new System.Drawing.Size(135, 27);
            this.cmbxDoctors.TabIndex = 30;
            // 
            // cmbxChild
            // 
            this.cmbxChild.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxChild.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbxChild.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxChild.FormattingEnabled = true;
            this.cmbxChild.Location = new System.Drawing.Point(550, 11);
            this.cmbxChild.Name = "cmbxChild";
            this.cmbxChild.Size = new System.Drawing.Size(135, 27);
            this.cmbxChild.TabIndex = 43;
            this.cmbxChild.SelectedIndexChanged += new System.EventHandler(this.cmbxChild_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(691, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 27);
            this.label3.TabIndex = 44;
            this.label3.Text = "المريض";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 123);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(839, 244);
            this.dataGridView1.TabIndex = 45;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // btnAddNewRes
            // 
            this.btnAddNewRes.Location = new System.Drawing.Point(596, 65);
            this.btnAddNewRes.Name = "btnAddNewRes";
            this.btnAddNewRes.Size = new System.Drawing.Size(217, 23);
            this.btnAddNewRes.TabIndex = 46;
            this.btnAddNewRes.Text = "اضافه حجز";
            this.btnAddNewRes.UseVisualStyleBackColor = true;
            this.btnAddNewRes.Click += new System.EventHandler(this.btnAddNewRes_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Location = new System.Drawing.Point(419, 65);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(171, 23);
            this.btnUpdate.TabIndex = 47;
            this.btnUpdate.Text = "تعديل حجز";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtNew
            // 
            this.txtNew.Enabled = false;
            this.txtNew.Location = new System.Drawing.Point(216, 65);
            this.txtNew.Name = "txtNew";
            this.txtNew.Size = new System.Drawing.Size(184, 23);
            this.txtNew.TabIndex = 48;
            this.txtNew.Text = "جديد";
            this.txtNew.UseVisualStyleBackColor = true;
            this.txtNew.Click += new System.EventHandler(this.txtNew_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(12, 65);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(198, 23);
            this.btnDelete.TabIndex = 49;
            this.btnDelete.Text = "حذف حجز";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Reservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 379);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.txtNew);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnAddNewRes);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cmbxChild);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbxDoctors);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Reservation";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "اداره الحجوزات";
            this.Load += new System.EventHandler(this.Reservation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker txtDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbxDoctors;
        private System.Windows.Forms.ComboBox cmbxChild;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnAddNewRes;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button txtNew;
        private System.Windows.Forms.Button btnDelete;
    }
}