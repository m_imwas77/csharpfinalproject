﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using final_project_mohammad_imwas_21610551.Enum;
namespace final_project_mohammad_imwas_21610551.model
{
    [Table("User")]
    class User
    {
        public int Id { get; set; }
        [Index("IX_FirstAndSecond", 1, IsUnique = true)]
        [StringLength(100)]
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public UserType Type { get; set; }
        public bool Active { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string Mojer { get; set; }
        
        public ICollection<Visit> Visits { get; set; }
    }
}
