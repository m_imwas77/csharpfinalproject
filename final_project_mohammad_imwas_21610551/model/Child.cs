﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using final_project_mohammad_imwas_21610551.Enum;
namespace final_project_mohammad_imwas_21610551.model
{
    class Child
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string DateOfBirth { get; set; }
        public ICollection<Guardian> Guardians { get; set; }
        public ICollection<Visit> visits;
    }
}
