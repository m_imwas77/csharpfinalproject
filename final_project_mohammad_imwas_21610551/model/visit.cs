﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using final_project_mohammad_imwas_21610551.Enum;
namespace final_project_mohammad_imwas_21610551.model
{
    class Visit
    {
        public int Id { get; set; }
        public string VisitDate { get; set; }
        public VisitStatus VisitStatus { get; set; }
        public User User { get; set; }
        public Child Child { set; get; }
        public ICollection<Examination> Examinations { get; set; }
        public ICollection<VisitDescription> VisitDescriptions { get; set; }
        public ICollection<Medicine> Medicines { get; set; }
    }
}
