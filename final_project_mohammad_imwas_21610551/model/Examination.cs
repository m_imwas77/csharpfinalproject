﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace final_project_mohammad_imwas_21610551.model
{
    class Examination
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Visit Visit { get; set; }
    }
}
