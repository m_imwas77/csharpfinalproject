﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace final_project_mohammad_imwas_21610551.model
{
    class ModelContext : DbContext
    {
        public ModelContext() : base("name=con") { }
        public DbSet<User> Users { set; get; }
        public DbSet<Child> Children { set; get; }
        public DbSet<Guardian> Guardians { get; set; }
        public DbSet<Visit> visits { get; set; }
        public DbSet<Examination> Examinations { get; set; }
        public DbSet<Medicine> Medicines { get; set; }
        public DbSet<VisitDescription> VisitDescriptions { get; set; }
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            //modelBuilder.Entity<User>()
            //            .HasOptional(s => s.Doctor)
            //            .WithRequired(r => r.User);

            //modelBuilder.Entity<User>()
            //            .HasOptional(s => s.Secretary)
            //            .WithRequired(r => r.User);

          
        }
    }
}
