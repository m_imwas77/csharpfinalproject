﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using final_project_mohammad_imwas_21610551.model;
using final_project_mohammad_imwas_21610551.Enum;
namespace final_project_mohammad_imwas_21610551
{
    public partial class View : Form
    {
        public View()
        {
            InitializeComponent();
        }
        List<SelectedVistes> selectedVistes;
        private void View_Load(object sender, EventArgs e)
        {

            cmbVistStatus.Items.Add("COMPLETED");
            cmbVistStatus.Items.Add("INPROGRESS");
            cmbVistStatus.Items.Add("LATE");
            cmbVistStatus.Items.Add("ALL");
            cmbVistStatus.SelectedIndex = cmbVistStatus.Items.Count - 1;


            using (ModelContext db = new ModelContext())
            {
                List<SelectedItem> children = db.Children.Select(p => new SelectedItem() { Id=p.Id,FirstName=p.FirstName}).ToList<SelectedItem>();
                
                children.Add(new SelectedItem() { Id = -2 , FirstName = "الجميع" });

                cmbxChild.DataSource = children;

                cmbxChild.ValueMember = "FirstName";
                cmbxChild.DisplayMember = "FirstName";
                cmbxChild.SelectedIndex = cmbxChild.Items.Count-1;

                List<SelectedItem> doctors = db.Users.Where(c=>c.Type == UserType.DOCTOR).Select(p => new SelectedItem() { Id = p.Id, FirstName = p.FirstName }).ToList<SelectedItem>();

                doctors.Add(new SelectedItem() { Id = -2, FirstName = "الجميع" });

                cmbxDoctors.DataSource = doctors;
                cmbxDoctors.ValueMember = "FirstName";
                cmbxDoctors.DisplayMember = "FirstName";
                cmbxDoctors.SelectedIndex = doctors.Count-1;

                Serch();
            }
        }

        private void ReRenderDataGridView()
        {
            dataGridView1.DataSource = selectedVistes;

            dataGridView1.Columns[0].HeaderText = "اسم الطفل";
            dataGridView1.Columns[1].HeaderText = "اسم الطبيب المشرف";
            dataGridView1.Columns[2].HeaderText = "تاريخ الزياره";
            dataGridView1.Columns[3].HeaderText = "حاله الزياره";
        }
       

        private void cmbVistStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            Serch();
        }

        private void cmbxChild_SelectedIndexChanged(object sender, EventArgs e)
        {
            Serch();
        }

        private void cmbxDoctors_SelectedIndexChanged(object sender, EventArgs e)
        {
            Serch();
        }

        private void Serch()
        {
            try
            {
                using (ModelContext db = new ModelContext())
                {
                    selectedVistes = db.visits
                                                .Where(c => c.User.Type == UserType.DOCTOR)
                                                .Select(p => new SelectedVistes()
                                                {
                                                    ChildName = p.Child.FirstName,
                                                    DoctorName = p.User.FirstName,
                                                    Date = p.VisitDate,
                                                    VisitStatus = p.VisitStatus
                                                }).ToList<SelectedVistes>();

                }

                if (cmbxChild.SelectedIndex != cmbxChild.Items.Count - 1)
                {
                    selectedVistes = selectedVistes.Where(c => c.ChildName == cmbxChild.SelectedValue.ToString()).ToList<SelectedVistes>();
                }

                if (cmbxDoctors.SelectedIndex != cmbxDoctors.Items.Count - 1)
                {
                    selectedVistes = selectedVistes.Where(c => c.DoctorName == cmbxDoctors.SelectedValue.ToString()).ToList<SelectedVistes>();
                }
                if (cmbVistStatus.SelectedIndex != cmbVistStatus.Items.Count - 1)
                {

                    if (cmbVistStatus.SelectedIndex == 0)
                    {
                        selectedVistes = selectedVistes.Where(c => c.VisitStatus == VisitStatus.COMPLETED).ToList<SelectedVistes>();
                    }
                    if (cmbVistStatus.SelectedIndex == 1)
                    {
                        selectedVistes = selectedVistes.Where(c => c.VisitStatus == VisitStatus.INPROGRESS).ToList<SelectedVistes>();
                    }                   
                    if(cmbVistStatus.SelectedIndex == 2)
                    {
                        selectedVistes = selectedVistes.Where(c => c.VisitStatus == VisitStatus.LATE).ToList<SelectedVistes>();
                    }
                    

                }
                if (chSingel.Checked)
                {
                    selectedVistes = selectedVistes.Where(c => c.Date == txtDateSingle.Value.ToShortDateString()).ToList<SelectedVistes>();

                }

                if (chdDauble.Checked)
                {
                    selectedVistes = selectedVistes.Where(c => 
                        DateTime.Parse(c.Date) >= DateTime.Parse(txtDateSingle.Value.ToShortDateString())  
                        && DateTime.Parse(c.Date) <= DateTime.Parse(txtDateDouble.Value.ToShortDateString()) ).ToList<SelectedVistes>();
                }
                ReRenderDataGridView();
            }
            catch (Exception)
            {

            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chdDauble.Checked) chdDauble.Checked = false;
            Serch();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (chSingel.Checked) chSingel.Checked = false;
            Serch();
        }
    }

    public class SelectedItem
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
    }

    public class SelectedVistes
    {
        public string  ChildName { get; set; }
        public string DoctorName { get; set; }
        public string Date { get; set; }
        public VisitStatus VisitStatus{ get; set; }
    }
}
