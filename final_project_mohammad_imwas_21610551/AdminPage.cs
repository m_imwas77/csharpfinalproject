﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using final_project_mohammad_imwas_21610551.model;
namespace final_project_mohammad_imwas_21610551
{
    public partial class AdminPage : Form
    {
         

        public AdminPage()
        {
            InitializeComponent();
        }

        private void btnUserModifay_Click(object sender, EventArgs e)
        {
            modifyUsers md = new modifyUsers();
            md.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ChRecord chRecord = new ChRecord();
            chRecord.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            EditChild editChild = new EditChild();
            editChild.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Reservation reservation = new Reservation();
            reservation.Show(); 
        }

        private void button4_Click(object sender, EventArgs e)
        {
            View view = new View();
            view.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }
    }
}
