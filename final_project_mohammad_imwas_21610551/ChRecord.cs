﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using final_project_mohammad_imwas_21610551.model;
using final_project_mohammad_imwas_21610551.Enum;
namespace final_project_mohammad_imwas_21610551
{
    public partial class ChRecord : Form
    {
        public ChRecord()
        {
            InitializeComponent();
        }

        private void ChRecord_Load(object sender, EventArgs e)
        {
            cmbxGender.Items.Add(Gender.MALE);
            cmbxGender.Items.Add(Gender.FEMALE);
            cmbxGender.SelectedIndex = 0;

            txtDOB.MaxDate = System.DateTime.Now.AddDays(-2);
            txtDOB.MinDate = System.DateTime.Now.AddYears(-100);

            txtDate.MinDate = System.DateTime.Now;
            txtDate.MaxDate = System.DateTime.Now.AddYears(100);

            using (ModelContext db = new ModelContext())
            {
                cmbxDoctors.DataSource = db.Users.Where(c => c.Type == UserType.DOCTOR).ToList<User>();
                cmbxDoctors.ValueMember = "id";
                cmbxDoctors.DisplayMember = "UserName";
            }
            if(cmbxDoctors.Items.Count == 0)
            {
                btnAdd.Enabled = false;
                btnAdd.Text = "لا يوجد اطباء الرجاء اضافه اطباء الى النظام";
            }
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (EditAddValidation("Add"))
            {
                return;
            }
            

            
            using (ModelContext db = new ModelContext())
            {
                Child child = new Child
                {
                    FirstName = txtChFirstName.Text,
                    LastName = txtChLastName.Text,
                    DateOfBirth = txtDOB.Text,
                    Gender = (cmbxGender.SelectedIndex == 0) ? Gender.MALE : Gender.FEMALE
                };
                Guardian guardian = new Guardian()
                {
                    FirstName = txtPeFirstName.Text,
                    LastName = txtPeLastName.Text,
                    PhoneNumber = txtPhoneNumber.Text,
                    Relationship = txtRelasion.Text,
                    Child = child
                };
                User user = db.Users.Where(c => c.Id.ToString() == cmbxDoctors.SelectedValue.ToString()).FirstOrDefault<User>();
                if (checkBox1.Checked)
                {
                    Visit visit = new Visit()
                    {
                        VisitDate = txtDate.Text,
                        VisitStatus = VisitStatus.INPROGRESS,
                        User = user,
                        Child = child
                    };

                    db.visits.Add(visit);
                }
                db.Guardians.Add(guardian);
                
                db.SaveChanges();
                MessageBox.Show("تم الحفظ بنجاح");
                ClearForme();
            }
        }
        private void ClearForme()
        {
            txtChFirstName.Text = "";
            txtChFirstName.Focus();
            txtChLastName.Text = "";
            txtPeFirstName.Text = "";
            txtPeLastName.Text = "";
            txtRelasion.Text = "";
        }
        private bool EditAddValidation(string typeOf)
        {
            int numberOfValidation = -6;
            //if (typeOf.ToUpper() == "EDIT")
            //{
            //    numberOfValidation = -5;
            //}

            int ShowErrorWhenAdd = 0;

            if (txtChFirstName.Text == "")
            {
                errorProvider1.SetError(txtChFirstName, "اسم المتخدم مطلوب");
                txtChFirstName.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtChFirstName, "");
                ShowErrorWhenAdd--;
            }

            if (txtPeFirstName.Text == "")
            {
                errorProvider1.SetError(txtPeFirstName, "اسم المتخدم مطلوب");
                txtPeFirstName.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtPeFirstName, "");
                ShowErrorWhenAdd--;
            }

            if (txtChLastName.Text == "")
            {
                errorProvider1.SetError(txtChLastName, "الاسم مطلوب");
                txtChLastName.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtChLastName, "");
                ShowErrorWhenAdd--;
            }

            if (txtPeLastName.Text == "")
            {
                errorProvider1.SetError(txtPeLastName, "الاسم  مطلوب");
                txtPeLastName.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtPeLastName, "");
                ShowErrorWhenAdd--;
            }

            if (!txtPhoneNumber.MaskCompleted)
            {
                errorProvider1.SetError(txtPhoneNumber, "اكمل رقم الهاتف");
                txtPhoneNumber.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtPhoneNumber, "");
                ShowErrorWhenAdd--;
            }

            if (txtRelasion.Text == "")
            {
                errorProvider1.SetError(txtRelasion, "القرابه مطلوبه");
                txtRelasion.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtRelasion, "");
                ShowErrorWhenAdd--;
            }
            if (ShowErrorWhenAdd > numberOfValidation) return true;
            return false;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
