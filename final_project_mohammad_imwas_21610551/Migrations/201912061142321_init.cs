﻿namespace final_project_mohammad_imwas_21610551.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Children",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Gender = c.Int(nullable: false),
                        DateOfBirth = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Guardians",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        PhoneNumber = c.String(),
                        Relationship = c.String(),
                        Child_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Children", t => t.Child_Id)
                .Index(t => t.Child_Id);
            
            CreateTable(
                "dbo.Doctors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false, storeType: "date"),
                        Gender = c.Int(nullable: false),
                        PhoneNumber = c.String(),
                        Mojer = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Workinghours",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DayName = c.String(),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        Doctor_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Doctors", t => t.Doctor_Id)
                .Index(t => t.Doctor_Id);
            
            CreateTable(
                "dbo.Examinations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Visit_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Visits", t => t.Visit_Id)
                .Index(t => t.Visit_Id);
            
            CreateTable(
                "dbo.Visits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VisitDate = c.DateTime(nullable: false),
                        VisitStatus = c.String(),
                        Child_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Children", t => t.Child_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.Child_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Medicines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Visit_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Visits", t => t.Visit_Id)
                .Index(t => t.Visit_Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(maxLength: 100),
                        PassWord = c.String(),
                        Type = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "IX_FirstAndSecond");
            
            CreateTable(
                "dbo.VisitDescriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Visit_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Visits", t => t.Visit_Id)
                .Index(t => t.Visit_Id);
            
            CreateTable(
                "dbo.Secretaries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Gender = c.Int(nullable: false),
                        PhoneNumber = c.String(),
                        Mojer = c.String(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Secretaries", "User_Id", "dbo.User");
            DropForeignKey("dbo.VisitDescriptions", "Visit_Id", "dbo.Visits");
            DropForeignKey("dbo.Visits", "User_Id", "dbo.User");
            DropForeignKey("dbo.Medicines", "Visit_Id", "dbo.Visits");
            DropForeignKey("dbo.Examinations", "Visit_Id", "dbo.Visits");
            DropForeignKey("dbo.Visits", "Child_Id", "dbo.Children");
            DropForeignKey("dbo.Workinghours", "Doctor_Id", "dbo.Doctors");
            DropForeignKey("dbo.Guardians", "Child_Id", "dbo.Children");
            DropIndex("dbo.Secretaries", new[] { "User_Id" });
            DropIndex("dbo.VisitDescriptions", new[] { "Visit_Id" });
            DropIndex("dbo.User", "IX_FirstAndSecond");
            DropIndex("dbo.Medicines", new[] { "Visit_Id" });
            DropIndex("dbo.Visits", new[] { "User_Id" });
            DropIndex("dbo.Visits", new[] { "Child_Id" });
            DropIndex("dbo.Examinations", new[] { "Visit_Id" });
            DropIndex("dbo.Workinghours", new[] { "Doctor_Id" });
            DropIndex("dbo.Guardians", new[] { "Child_Id" });
            DropTable("dbo.Secretaries");
            DropTable("dbo.VisitDescriptions");
            DropTable("dbo.User");
            DropTable("dbo.Medicines");
            DropTable("dbo.Visits");
            DropTable("dbo.Examinations");
            DropTable("dbo.Workinghours");
            DropTable("dbo.Doctors");
            DropTable("dbo.Guardians");
            DropTable("dbo.Children");
        }
    }
}
