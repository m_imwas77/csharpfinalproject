﻿namespace final_project_mohammad_imwas_21610551.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixVisted : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Visits", "VisitStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Visits", "VisitStatus", c => c.String());
        }
    }
}
