﻿namespace final_project_mohammad_imwas_21610551.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Doctors", "Id", "dbo.User");
            DropForeignKey("dbo.Workinghours", "Doctor_Id", "dbo.Doctors");
            DropForeignKey("dbo.Secretaries", "User_Id", "dbo.User");
            DropIndex("dbo.Doctors", new[] { "Id" });
            DropIndex("dbo.Workinghours", new[] { "Doctor_Id" });
            DropIndex("dbo.Secretaries", new[] { "User_Id" });
            AddColumn("dbo.User", "FirstName", c => c.String());
            AddColumn("dbo.User", "LastName", c => c.String());
            AddColumn("dbo.User", "DateOfBirth", c => c.DateTime(nullable: false, storeType: "date"));
            AddColumn("dbo.User", "Gender", c => c.Int(nullable: false));
            AddColumn("dbo.User", "PhoneNumber", c => c.String());
            AddColumn("dbo.User", "Mojer", c => c.String());
            AddColumn("dbo.Workinghours", "User_Id", c => c.Int());
            CreateIndex("dbo.Workinghours", "User_Id");
            AddForeignKey("dbo.Workinghours", "User_Id", "dbo.User", "Id");
            DropColumn("dbo.Workinghours", "Doctor_Id");
            DropTable("dbo.Doctors");
            DropTable("dbo.Secretaries");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Secretaries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Gender = c.Int(nullable: false),
                        PhoneNumber = c.String(),
                        Mojer = c.String(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Doctors",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false, storeType: "date"),
                        Gender = c.Int(nullable: false),
                        PhoneNumber = c.String(),
                        Mojer = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Workinghours", "Doctor_Id", c => c.Int());
            DropForeignKey("dbo.Workinghours", "User_Id", "dbo.User");
            DropIndex("dbo.Workinghours", new[] { "User_Id" });
            DropColumn("dbo.Workinghours", "User_Id");
            DropColumn("dbo.User", "Mojer");
            DropColumn("dbo.User", "PhoneNumber");
            DropColumn("dbo.User", "Gender");
            DropColumn("dbo.User", "DateOfBirth");
            DropColumn("dbo.User", "LastName");
            DropColumn("dbo.User", "FirstName");
            CreateIndex("dbo.Secretaries", "User_Id");
            CreateIndex("dbo.Workinghours", "Doctor_Id");
            CreateIndex("dbo.Doctors", "Id");
            AddForeignKey("dbo.Secretaries", "User_Id", "dbo.User", "Id");
            AddForeignKey("dbo.Workinghours", "Doctor_Id", "dbo.Doctors", "Id");
            AddForeignKey("dbo.Doctors", "Id", "dbo.User", "Id");
        }
    }
}
