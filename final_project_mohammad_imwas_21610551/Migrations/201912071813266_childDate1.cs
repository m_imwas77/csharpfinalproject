﻿namespace final_project_mohammad_imwas_21610551.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class childDate1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Visits", "VisitDate", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Visits", "VisitDate", c => c.DateTime(nullable: false));
        }
    }
}
