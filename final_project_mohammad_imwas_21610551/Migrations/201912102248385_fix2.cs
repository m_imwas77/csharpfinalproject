﻿namespace final_project_mohammad_imwas_21610551.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Workinghours", "User_Id", "dbo.User");
            DropIndex("dbo.Workinghours", new[] { "User_Id" });
            DropTable("dbo.Workinghours");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Workinghours",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DayName = c.String(),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Workinghours", "User_Id");
            AddForeignKey("dbo.Workinghours", "User_Id", "dbo.User", "Id");
        }
    }
}
