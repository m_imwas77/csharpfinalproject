﻿namespace final_project_mohammad_imwas_21610551.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class one_to_one : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Workinghours", "Doctor_Id", "dbo.Doctors");
            DropPrimaryKey("dbo.Doctors");
            AlterColumn("dbo.Doctors", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Doctors", "Id");
            CreateIndex("dbo.Doctors", "Id");
            AddForeignKey("dbo.Doctors", "Id", "dbo.User", "Id");
            AddForeignKey("dbo.Workinghours", "Doctor_Id", "dbo.Doctors", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Workinghours", "Doctor_Id", "dbo.Doctors");
            DropForeignKey("dbo.Doctors", "Id", "dbo.User");
            DropIndex("dbo.Doctors", new[] { "Id" });
            DropPrimaryKey("dbo.Doctors");
            AlterColumn("dbo.Doctors", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Doctors", "Id");
            AddForeignKey("dbo.Workinghours", "Doctor_Id", "dbo.Doctors", "Id");
        }
    }
}
