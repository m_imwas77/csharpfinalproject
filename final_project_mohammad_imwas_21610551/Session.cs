﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using final_project_mohammad_imwas_21610551.model;
using final_project_mohammad_imwas_21610551.Enum;
namespace final_project_mohammad_imwas_21610551
{
    public partial class Session : Form
    {

        IList<SessionDetails> sessionDetails ;
        public Session()
        {
            InitializeComponent();
        }

        private void Session_Load(object sender, EventArgs e)
        {
            dateTimePicker1.MinDate = System.DateTime.Now.AddDays(1);
            ComboBox CB = new ComboBox();
            CB.Items.Add("Description");
            CB.Items.Add("Medicine");
            CB.Items.Add("Examination");
            CB.SelectedIndex = 0;
            ((DataGridViewComboBoxColumn)dataGridView1.Columns[2]).DataSource = CB.Items;
            txtDOB.Enabled = false;

            sessionDetails = new List<SessionDetails>();
            ReRednerComboBoxForChild();
        }

        private void ReRednerComboBoxForChild()
        {
            using (ModelContext db = new ModelContext())
            {

                string DateTody = System.DateTime.Now.ToShortDateString();
                List<SelectedChild> selectedChildren =
                    db.visits.Where(c => c.User.Id == Program.LoginUserId
                                    && c.VisitDate == DateTody
                                    && c.VisitStatus == VisitStatus.INPROGRESS)
                                    .Select(p => new SelectedChild()
                                    {
                                        Id = p.Child.Id,
                                        FirstName = p.Child.FirstName
                                    }).ToList<SelectedChild>();

                cmbxChild.DataSource = selectedChildren;
                cmbxChild.DisplayMember = "FirstName";
                cmbxChild.ValueMember = "Id";
                cmbxChild.SelectedIndex = -1;
                if(selectedChildren.Count == 0)
                {
                    btnAdd.Enabled = false;
                    cmbxChild.Enabled = false;
                    dataGridView1.Enabled = false;
                    btnAdd.Text = "لم يعد لديك اي مراجعين اليوم";
                }
            }
        }
        private void cmbxChild_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (ModelContext db = new ModelContext())
            {
                try
                {

                    Child child = db.Children.Where(c => c.Id.ToString() == cmbxChild.SelectedValue.ToString()).FirstOrDefault<Child>();
                    if (child != null)
                    {
                        string d = "";
                        txtFirtName.Text =  child.FirstName;
                        txtLastName.Text =  child.LastName ;
                        txtGender.Text =  (child.Gender == Gender.MALE) ? "male"  : "female" ;
                        txtDOB.Text =  child.DateOfBirth ;
                        dataGridView1.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("لم يتم اختيار اي طفل");
                    }
               }
               catch (Exception) { }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("اتمام المراجعه", "هل انت متاكد", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (result == DialogResult.Yes)
            {
                    using (ModelContext db = new ModelContext())
                    {
                        Visit visit = db.visits.Where(c => c.Child.Id.ToString() == cmbxChild.SelectedValue.ToString() && c.VisitStatus == VisitStatus.INPROGRESS).FirstOrDefault<Visit>();
                    if (visit != null)
                    {
                        visit.VisitStatus = VisitStatus.COMPLETED;
                        db.SaveChanges();
                        foreach (DataGridViewRow item in dataGridView1.Rows)
                        {
                            if (item.Cells[0].FormattedValue.ToString() == "") break;
                            if (item.Cells[1].FormattedValue.ToString() == "") break;
                            if (item.Cells[2].FormattedValue.ToString() == "") break;
                            if (item.Cells[2].FormattedValue.ToString() == "Description")
                            {
                                VisitDescription description = new VisitDescription()
                                {
                                    Name = item.Cells[0].FormattedValue.ToString(),
                                    Description = item.Cells[1].FormattedValue.ToString(),
                                    Visit = visit
                                };
                                db.VisitDescriptions.Add(description);
                                db.SaveChanges();
                            }
                            else if (item.Cells[2].FormattedValue.ToString() == "Medicine")
                            {
                                Medicine medicine = new Medicine()
                                {
                                    Name = item.Cells[0].FormattedValue.ToString(),
                                    Description = item.Cells[1].FormattedValue.ToString(),
                                    Visit = visit
                                };
                                db.Medicines.Add(medicine);
                                db.SaveChanges();
                            }
                            else
                            {
                                Examination examination = new Examination()
                                {
                                    Name = item.Cells[0].FormattedValue.ToString(),
                                    Description = item.Cells[1].FormattedValue.ToString(),
                                    Visit = visit
                                };
                                db.Examinations.Add(examination);
                                db.SaveChanges();
                            }

                           
                        }
                        if (checkBox1.Checked == true)
                        {
                            Child child = db.Children.Where(c => c.Id.ToString() == cmbxChild.SelectedValue.ToString()).FirstOrDefault<Child>();
                            User user = db.Users.Where(c => c.Id.ToString() == Program.LoginUserId.ToString()).FirstOrDefault<User>();

                            Visit visit1 = new Visit()
                            {
                                VisitDate = dateTimePicker1.Value.ToShortDateString(),
                                VisitStatus = VisitStatus.INPROGRESS,
                                Child = child,
                                User = user
                            };
                            db.visits.Add(visit1);
                            db.SaveChanges();
                        }
                        ReRednerComboBoxForChild();
                        dataGridView1.Rows.Clear();
                        dataGridView1.Enabled = false;
                        txtFirtName.Text = "";
                        txtLastName.Text = "";
                        txtGender.Text = "";
                        txtDOB.Text = "";

                       
                        checkBox1.Checked = false;
                        MessageBox.Show("تم الحفظ بنجاح");
                    }
                    else
                    {
                        MessageBox.Show("لم يتم العثور على زياره");
                    }
                }
            }
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if(e.FormattedValue.ToString() == "")
            {
                e.Cancel = true;
                dataGridView1.Rows[e.RowIndex].ErrorText = "القيمه مطولبه";
            }
            else
            {
                dataGridView1.Rows[e.RowIndex].ErrorText = "";
            }
        }
    }
    public class SelectedChild
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
    }
    public class SessionDetails
    {
        public string  Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
    }
}
