﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using final_project_mohammad_imwas_21610551.model;
using final_project_mohammad_imwas_21610551.Enum;
namespace final_project_mohammad_imwas_21610551
{
    public partial class Reservation : Form
    {
        public Reservation()
        {
            InitializeComponent();
        }
        private void ReRenderDataGridView()
        {
            using (ModelContext db = new ModelContext())
            {
                dataGridView1.DataSource = db.visits.Where(c => c.Child.Id.ToString() == cmbxChild.SelectedValue.ToString()).OrderBy(c=>c.Id).ToList<Visit>();

                ConfigerDataGridView();
            }
        }
        private void ConfigerDataGridView()
        {
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "التاريخ";
            dataGridView1.Columns[2].HeaderText = "حاله الحجز";
            dataGridView1.Columns[3].Visible = false;
            dataGridView1.Columns[4].Visible = false;
            dataGridView1.Columns[5].Visible = false;
            dataGridView1.Columns[6].Visible = false;
            dataGridView1.Columns[7].Visible = false;
        }
        private void Reservation_Load(object sender, EventArgs e)
        {

         
            using (ModelContext db = new ModelContext())
            {
                cmbxChild.DataSource = db.Children.ToList<Child>();

                cmbxChild.ValueMember = "id";
                cmbxChild.DisplayMember = "FirstName";
                try
                {
                    cmbxChild.SelectedIndex = 0;
                }catch(Exception ) { }
                cmbxDoctors.DataSource = db.Users.Where(c => c.Type == UserType.DOCTOR).ToList<User>();
                cmbxDoctors.ValueMember = "id";
                cmbxDoctors.DisplayMember = "FirstName";
                cmbxDoctors.SelectedIndex = 0;

                try
                {

                    List<Visit> visits = db.visits.Where(c => c.Child.Id.ToString() == cmbxChild.SelectedValue.ToString()).ToList<Visit>();
                    dataGridView1.DataSource = visits;
                    ConfigerDataGridView();
                }
                catch (Exception) { }
               
            }

            txtDate.Value = System.DateTime.Now;
            txtDate.MinDate = System.DateTime.Now.AddYears(-100);
            txtDate.MaxDate = System.DateTime.Now.AddYears(100);
        }

        private void cmbxChild_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (ModelContext db = new ModelContext())
            {
                try
                { 
                    dataGridView1.DataSource = db.visits.Where(c => c.Child.Id.ToString() == cmbxChild.SelectedValue.ToString()).ToList<Visit>();

                    ConfigerDataGridView();
                }
                catch (Exception)
                {

                }
            }
        }

        private void btnAddNewRes_Click(object sender, EventArgs e)
        {
            try
            {
                if ((txtDate.Value.ToShortDateString() == System.DateTime.Now.ToShortDateString()) || txtDate.Value > System.DateTime.Now)
                {

                    using (ModelContext db = new ModelContext())
                    {
                        Child child = db.Children.Where(c => c.Id.ToString() == cmbxChild.SelectedValue.ToString()).FirstOrDefault<Child>();
                        User user = db.Users.Where(c => c.Id.ToString() == cmbxDoctors.SelectedValue.ToString()).FirstOrDefault<User>();
                        Visit visit = new Visit()
                        {
                            VisitDate = txtDate.Text,
                            VisitStatus = VisitStatus.INPROGRESS,
                            Child = child,
                            User = user
                        };
                        db.visits.Add(visit);
                        db.SaveChanges();
                        ReRenderDataGridView();
                        MessageBox.Show("تم الاضافه بنجاح");
                        //    ConfigerDataGridView();
                    }
                }
                else
                {
                    MessageBox.Show("الرجاء اختيار تاريخ صحيح");
                    return;

                }
            }
            catch (Exception) { MessageBox.Show("لم يتم اختيار مريض"); }
        }
        string Id = "";
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1)
            {
                try
                {
                    
                    txtDate.Text = dataGridView1.Rows[e.RowIndex].Cells[1].FormattedValue.ToString();
                    Id = dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                    btnUpdate.Enabled = true;
                    txtNew.Enabled = true;
                    btnDelete.Enabled = true;
                    cmbxChild.Enabled = false;
                    btnAddNewRes.Enabled = false;
                }
                catch (Exception)
                {
                    MessageBox.Show("الرجاء اختيار عنصر واحد");
                }
            }
            
        }

        private void txtNew_Click(object sender, EventArgs e)
        {
            cmbxDoctors.SelectedIndex = 0;
            txtDate.Value = System.DateTime.Now;
            btnUpdate.Enabled = false;
            txtNew.Enabled = false;
            btnDelete.Enabled = false;
            cmbxChild.Enabled = true;
            btnAddNewRes.Enabled = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("حذف", "هل انت متاكد من الحذ", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if(result == DialogResult.Yes)
            {
                using (ModelContext db = new ModelContext())
                {
                    Visit visit = db.visits.Where(c => c.Id.ToString() == Id).FirstOrDefault<Visit>();
                    if (visit != null)
                    {
                        IList<Medicine> medicines = db.Medicines.Where(c => c.Visit.Id == visit.Id).ToList<Medicine>();
                        IList<Examination> examinations = db.Examinations.Where(c => c.Visit.Id == visit.Id).ToList<Examination>();
                        IList<VisitDescription> visitDescriptions = db.VisitDescriptions.Where(c => c.Visit.Id == visit.Id).ToList<VisitDescription>();
                        foreach (var item in medicines)
                        {
                            db.Medicines.Remove(item);
                        }
                        foreach (var item in examinations)
                        {
                            db.Examinations.Remove(item);
                        }
                        foreach (var item in visitDescriptions)
                        {
                            db.VisitDescriptions.Remove(item);
                        }

                        db.SaveChanges();
                        db.visits.Remove(visit);
                        db.SaveChanges();
                        ReRenderDataGridView();
                        MessageBox.Show("تم الحذف بنجاح");
                    }
                    else
                    {
                        MessageBox.Show("لم يتم العثور على بيانات");
                    }
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        { 
            
            if ((txtDate.Value.ToShortDateString() == System.DateTime.Now.ToShortDateString()) || txtDate.Value > System.DateTime.Now)
            {
                using (ModelContext db = new ModelContext())
                {
                    Visit visit = db.visits.Where(c => c.Id.ToString() == Id).FirstOrDefault<Visit>();
                    if (visit != null)
                    {
                        visit.VisitDate = txtDate.Text;
                        visit.User = db.Users.Where(c => c.Id.ToString() == cmbxDoctors.SelectedValue.ToString()).FirstOrDefault<User>();
                        db.visits.Add(visit);
                        db.SaveChanges();
                        ReRenderDataGridView();
                        MessageBox.Show("تم التعديل بنجاح");
                    }
                    else
                    {
                        MessageBox.Show("لم يتم العثور على بيانات");
                    }
                }
            }
            else
            {
                MessageBox.Show("الرجاء اختيار تاريخ صحيح");
                return;
            }
        }
    }
}
