﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using final_project_mohammad_imwas_21610551.model;
using final_project_mohammad_imwas_21610551.Enum;
namespace final_project_mohammad_imwas_21610551
{
    public partial class EditChild : Form
    {
        public EditChild()
        {
            InitializeComponent();
        }

        private void EditChild_Load(object sender, EventArgs e)
        {
            cmbxGender.Items.Add(Gender.MALE);
            cmbxGender.Items.Add(Gender.FEMALE);
            //mbxGender.SelectedIndex = 0;

            txtDOB.MaxDate = System.DateTime.Now.AddDays(-2);
            txtDOB.MinDate = System.DateTime.Now.AddYears(-120);

            using (ModelContext db = new ModelContext())
            {
                cmbxChild.DataSource = db.Children.ToList<Child>();

                cmbxChild.ValueMember = "id";
                cmbxChild.DisplayMember = "FirstName";
            }
        }

        private void cmbxChild_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        private void ReREnderDataGrid()
        {
            using (ModelContext db = new ModelContext()) {
                try
                {
                    dataGridView1.DataSource = db.Guardians.Where(c => c.Child.Id.ToString() == cmbxChild.SelectedValue.ToString()).ToList<Guardian>();
                       dataGridView1.Columns[0].Visible = false;
                    dataGridView1.Columns.Remove("Child");
                    dataGridView1.Columns[1].HeaderText = "اسم الاول";
                    dataGridView1.Columns[2].HeaderText = "اسم العائله";
                    dataGridView1.Columns[3].HeaderText = "رقم الهاتف";
                    dataGridView1.Columns[4].HeaderText = "العلاقه";
                }
                catch (Exception )
                {
                    dataGridView1.Rows.Clear();
                }
                
            }
        }
        string  Id = "-1";
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1)
            {
                try
                {
                    Id = dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                    txtPeFirstName.Text = dataGridView1.Rows[e.RowIndex].Cells[1].FormattedValue.ToString();
                    txtPeLastName.Text = dataGridView1.Rows[e.RowIndex].Cells[2].FormattedValue.ToString();
                    txtPhoneNumber.Text = dataGridView1.Rows[e.RowIndex].Cells[3].FormattedValue.ToString();
                    txtRelasion.Text = dataGridView1.Rows[e.RowIndex].Cells[4].FormattedValue.ToString();
                    btnDelete.Enabled = true;
                    btnNew.Enabled = true;
                    btnUpdate.Enabled = true;
                    btnAdd.Enabled = false;
                }
                catch (Exception)
                {
                    MessageBox.Show("الرجاء اختيار عنصر واحد");
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            
        }

        private void ClearForme()
        {
            txtPeFirstName.Text = "";
            txtPeLastName.Text = "";
            txtPhoneNumber.Text = "";
            txtRelasion.Text = "";
            btnDelete.Enabled = !true;
            btnNew.Enabled = !true;
            btnUpdate.Enabled = !true;
            btnAdd.Enabled = !false;
            Id = "-1";
        }

        private bool EditAddValidation(string typeOf)
        {
            int numberOfValidation = -3;
            //if (typeOf.ToUpper() == "EDIT")
            //{
            //    numberOfValidation = -5;
            //}

            int ShowErrorWhenAdd = 0;

           
            if (txtPeFirstName.Text == "")
            {
                errorProvider1.SetError(txtPeFirstName, "اسم المتخدم مطلوب");
                txtPeFirstName.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtPeFirstName, "");
                ShowErrorWhenAdd--;
            }

            if (txtPeLastName.Text == "")
            {
                errorProvider1.SetError(txtPeLastName, "الاسم مطلوب");
                txtPeLastName.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtPeLastName, "");
                ShowErrorWhenAdd--;
            }

            if (!txtPhoneNumber.MaskCompleted)
            {
                errorProvider1.SetError(txtPhoneNumber, "اكمل رقم الهاتف");
                txtPhoneNumber.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtPhoneNumber, "");
                ShowErrorWhenAdd--;
            }

            if (txtRelasion.Text == "")
            {
                errorProvider1.SetError(txtRelasion, "القرابه مطلوبه");
                txtRelasion.Focus();
                ShowErrorWhenAdd++;
            }
            else
            {
                errorProvider1.SetError(txtRelasion, "");
                ShowErrorWhenAdd--;
            }
            if (ShowErrorWhenAdd > numberOfValidation) return true;
            return false;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
           
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
           
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            
        }

        private void btnChEdit_Click(object sender, EventArgs e)
        {
            
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void cmbxChild_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            using (ModelContext db = new ModelContext())
            {
                try
                {

                    Child child = db.Children.Where(c => c.Id.ToString() == cmbxChild.SelectedValue.ToString()).FirstOrDefault<Child>();

                    if (child != null)
                    {
                        txtChFirstName.Text = child.FirstName;
                        txtChLastName.Text = child.LastName;
                        txtDOB.Text = child.DateOfBirth;
                        cmbxGender.SelectedIndex = (child.Gender == Gender.MALE) ? 0 : 1;
                    }

                    ReREnderDataGrid();

                }
                catch (Exception)
                {

                }

            }
        }

        private void btnChEdit_Click_1(object sender, EventArgs e)
        {
           
            if (txtChFirstName.Text == "")
            {
                errorProvider1.SetError(txtChFirstName, "الاسم مطلوب");
                txtChFirstName.Focus();
                return;
            }
            else
            {
                errorProvider1.SetError(txtChFirstName, "");
            }

            if (txtChLastName.Text == "")
            {
                errorProvider1.SetError(txtChLastName, "الاسم مطلوب");
                txtChLastName.Focus();
                return;
            }
            else
            {
                errorProvider1.SetError(txtChLastName, "");
            }
            using (ModelContext db = new ModelContext())
            {
                try
                {
                    string index = cmbxChild.SelectedValue.ToString();
                    Child child = db.Children.Where(c => c.Id.ToString() == index).FirstOrDefault<Child>();
                    if (child != null)
                    {
                        child.FirstName = txtChFirstName.Text;
                        child.LastName = txtChLastName.Text;
                        child.DateOfBirth = txtDOB.Text;
                        child.Gender = (cmbxGender.SelectedIndex == 0) ? Gender.MALE : Gender.FEMALE;
                        db.SaveChanges();
                    }
                    else
                    {
                        MessageBox.Show("لم يتم اختيار اي طفل");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("لم يتم اختيار اي طفل");

                }
            }
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            using (ModelContext db = new ModelContext())
            {
                if (Id != "-1")
                {
                    DialogResult result = MessageBox.Show("هل انت متاكد من الحذف", "حذف مستخدم", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                    if (result == DialogResult.Yes)
                    {
                        Guardian guardian = db.Guardians.Where(c => c.Id.ToString() == Id).FirstOrDefault<Guardian>();
                        db.Guardians.Remove(guardian);
                        db.SaveChanges();
                        ReREnderDataGrid();
                        ClearForme();
                    }
                }
                else
                {
                    MessageBox.Show("لم يتم اختيار حقل");
                }
            }
        }

        private void btnUpdate_Click_1(object sender, EventArgs e)
        {
            if (EditAddValidation("EDIT"))
            {
                return;
            }
            using (ModelContext db = new ModelContext())
            {
                if (Id != "-1")
                {
                    Guardian guardian = db.Guardians.Where(c => c.Id.ToString() == Id).FirstOrDefault<Guardian>();
                    if (guardian != null)
                    {
                        guardian.FirstName = txtPeFirstName.Text;
                        guardian.LastName = txtPeLastName.Text;
                        guardian.PhoneNumber = txtPhoneNumber.Text;
                        guardian.Relationship = txtRelasion.Text;
                        db.SaveChanges();
                        ReREnderDataGrid();
                        ClearForme();
                    }
                    else
                    {
                        MessageBox.Show("لم يتم اختيار حقل");
                    }
                }
                else
                {
                    MessageBox.Show("لم يتم اختيار حقل");
                }
            }
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            if (EditAddValidation("EDIT"))
            {
                return;
            }

            using (ModelContext db = new ModelContext())
            {
                try
                {
                    Child child = db.Children.Where(c => c.Id.ToString() == cmbxChild.SelectedValue.ToString()).FirstOrDefault<Child>();

                    Guardian guardian = new Guardian()
                    {
                        FirstName = txtPeFirstName.Text,
                        LastName = txtPeLastName.Text,
                        PhoneNumber = txtPhoneNumber.Text,
                        Relationship = txtRelasion.Text,
                        Child = child
                    };

                    db.Guardians.Add(guardian);
                    db.SaveChanges();

                    ClearForme();
                    ReREnderDataGrid();
                }
                catch (Exception)
                {
                    MessageBox.Show("لم يتم اختيار اي طفل");
                }
               
            }
        }

        private void btnNew_Click_1(object sender, EventArgs e)
        {
            ClearForme();
        }

        private void btnDeleteChild_Click(object sender, EventArgs e)
        {
            using (ModelContext db = new ModelContext())
            {
               try
               {
                    string index = cmbxChild.SelectedValue.ToString();
                    Child child = db.Children.Where(c => c.Id.ToString() == index).FirstOrDefault<Child>();

                    if (child != null)
                    {
                        DialogResult result = MessageBox.Show("هل انت متاكد من الحذف", "حذف مستخدم", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (result == DialogResult.Yes)
                        {
                            List<Guardian> guardians = db.Guardians.Where(c=>c.Child.Id == child.Id).ToList<Guardian>();
                            foreach (var item in guardians)
                            {
                                db.Guardians.Remove(item);
                            }
                        List<Visit> visits = db.visits.Where(c => c.Child.Id == child.Id).ToList<Visit>();
                        foreach (var item in visits)
                        {
                            List<VisitDescription> visitDescriptions = db.VisitDescriptions.Where(c => c.Visit.Id == item.Id).ToList<VisitDescription>();
                            List<Medicine> medicines = db.Medicines.Where(c => c.Visit.Id == item.Id).ToList<Medicine>();
                            List<Examination> examinations = db.Examinations.Where(c => c.Visit.Id == item.Id).ToList<Examination>();
                            foreach (var item1 in visitDescriptions)
                            {
                                db.VisitDescriptions.Remove(item1);
                            }
                            foreach (var item2 in medicines)
                            {
                                db.Medicines.Remove(item2);
                            }
                            foreach (var item3 in examinations)
                            {
                                db.Examinations.Remove(item3);
                            }
                            db.SaveChanges();
                            db.visits.Remove(item);
                        }
                        db.SaveChanges();
                        db.SaveChanges();
                        db.Children.Remove(child);
                        db.SaveChanges();
                        cmbxChild.DataSource = db.Children.ToList<Child>();

                            cmbxChild.ValueMember = "id";
                            cmbxChild.DisplayMember = "FirstName";

                            ReREnderDataGrid();
                        }
                    }
                    else
                    {
                        MessageBox.Show("لم يتم اختيار اي طفل");
                    }
               }
               catch (Exception)
               {
                    MessageBox.Show("لم يتم اختيار اي طفل");

               }
            }
        }
    }
}
